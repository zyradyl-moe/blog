---
title: Reinitializing...
date: 2020-05-02
---

Welcome back.

As those few people who routinely check the site may have noticed,
[zyradyl.moe][1] dropped off the face of the internet for a period of about
four months. If you're just trying to access information that was previously
hosted here, you can find it over on the [archive][2]. This website will be
expanded in time to encompass far more than it originally did.

For those curious as to what happened, read on...

### Identity Crisis ###

While I don't often talk about myself online, suffice to say that I went
through a legal name change last year. My previous registrar's process for
handling such a thing did not take into account that someone may be in the
middle of one when it came time for re-registration, and thus may have several
documents in a state of flux.

Thus, with identity documents and finance documents slightly out of sync, I
ended up in a situation where I couldn't easily renew. This coincided with a
fairly significant job change, and a sort of "lifestyle reconfiguration" and
thus the website just... slipped through the cracks. By the time I realized
what had happened the domain had gone into what the registrar called
"restorable" mode, but which I have seen called "vaulted" mode, where they will
charge you an additional fee to get the domain back.

In my case, this fee was three times the cost of the domain in the first place,
bringing the total order up to four times the initial registration cost, which
is frankly ridiculous. At this point I just decided to let the domain fully
lapse and I would re-register it through the domain registrar I had initially
wanted in the first place. By that time, I figured that I would be free enough
time wise to invest in recreating the website.

Despite that time frame occurring right during the COVID-19 pandemic, I have
managed to find enough tim to get started on this project.

### Where do we go from here? ###

Well, the old site is hosted on a new subdomain, and it will remain there in a
read only state for the foreseeable future. This allows me to operate at my own
pace, as no information has actually been removed at this point.

As for the domain as a whole, I have several plans for it, including launching
a public discord where people will be able to talk to me.

There were a good number of projects hosted under the domain previously that
were not publicly accessible. This will be changing, and most of my projects,
however silly, will be made publicly available.

In otherwords, stay tuned, and thanks for sticking around. Analytics through
Cloudflare reveals there are quite a few of you guys.

Thanks.

[1]: https://zyradyl.moe
[2]: https://archive.zyradyl.moe
